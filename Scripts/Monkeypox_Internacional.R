#------------------------------------------------------------------------------#
# ------------------ UIES MONKEYPOX INTERNATIONAL ---------------------------- #
#------------------------------------------------------------------------------#

# -----------------------------------------------------------------------------#
#           1. CARGA DE PAQUETES, LIBRERÍAS Y FUNCIONES ####
#------------------------------------------------------------------------------#

# Cargamos librerías y paquetes a emplear
if (!require("pacman")) install.packages("pacman")
pacman::p_load(tidyverse,data.table,plotly, aweek, lubridate, ggrepel, sp,
               here,maps, maptools, leaflet, ggpubr, leaflegend, officer,
               magrittr, mapview, flextable, rvest, rgeos, htmlwidgets, extrafont)

#Establecemos el path principal
path <- here::here()
path_Productos <- paste0(path, "/productos/")

#Se establecen dos directorios principales}
if(!dir.exists(paste0(path,"/Scripts/"))){
  dir.create(paste0(path,"/Scripts/"))
}else{
  print("Directorio Scripts existente")
}

if(!dir.exists(paste0(path,"/productos/"))){
  dir.create(paste0(path,"/productos/"))
}else{
  print("Directorio productos existente")
}

if(!dir.exists(paste0(path,"/bin/"))){
  dir.create(paste0(path,"/bin/"))
}



#### INSTALACIÓN DE LA FUENTE MONTSERRAT AUTOMÁTICAMENTE ####
if(!require("extrafont")){
  print("No tenemos fuente. instalando")
  install.packages("extrafont")

  if(!is.null(windowsFonts()[["Montserrat"]])){
    print("Omitiendo instalación de fuente Montserrat")

  }else{
    print("No tenemos la fuente Montserrat, se instalará...")
    font_import(paths = paste0(path,"/bin/"))
    loadfonts()
  }

}else{
  print("Ya tenemos fuente Montserrat")

}

source(paste0(path, "/Scripts/funciones_Monkepox_Internacional.R"))

# -----------------------------------------------------------------------------#
#           2. REGISTROS Y VALORES GENERALES DE COLORES ####
#------------------------------------------------------------------------------#

#Establecemos una gama de colores que servirán para los colores del mapa

# Cada color corresponde a una categoría
cat_1 <- "#A7874A"
cat_2 <- "#C6A565"
cat_3 <- "#D9BB7A"
cat_4 <- "#ECD08E"
cat_5 <- "#FFE5A3"
cat_6 <- "#FFFFFF"

colores_fact<- c("Europa" = "#621132",
                 "América" = "#9D2449",
                 "África" = "#28614D",
                 "Pacífico Occidental" = "#D4C19C",
                 "Mediterráneo Oriental" = "#7F7F7F",
                 "Asia Sudoriental" = "#B38E5D")

#Estas variables sirven para delimitar el factor de las catgegorías
#Aqui se tienen que cambiar estos rangos dependiendo de los casos y la distribución que presenten
# Estos tres bloques tienen que corresponder según el rango

casos5 <- 25000
casos4 <- 24999
casos3 <- 9999
casos2 <- 999
casos1 <- 99

casos5_ <- "> 25000"
casos4_ <- "10000 - 24999"
casos3_ <- "1000 - 9999"
casos2_ <- "100 - 999"
casos1_ <- "1 - 99"
casos0_ <- "Sin registro"

niveles <- c("> 25000",
             "10000 - 24999",
             "1000 - 9999",
             "100 - 999",
             "1 - 99",
             "Sin registro")

#Establecemos la relación de color de las categorías de cada país
colores_categorias<-c(casos5_ = cat_1,
                      casos4_ = cat_2,
                      casos3_ = cat_3,
                      casos2_ = cat_4,
                      casos1_ = cat_5,
                      casos0_ = cat_6)


# -----------------------------------------------------------------------------#
#           3. DATOS DE VIRUELA DE OMS POR PAÍS ####
#------------------------------------------------------------------------------#
#load_data_OMS()[1] es una función que permite obtener el dataframe de la página
#Se realiza mediante un proceso de webscrapping a la tabla principal Creamos la
#variable de la categoría con respecto a los casos que previamente definimos en
#los bloques principales.

Datos_Viruela <- load_data_OMS()[1] %>%
  as.data.frame() %>%
  mutate(Categoria = case_when( Casos >= casos5 ~ casos5_,
                                (Casos >= casos3+1 & Casos <= casos4) ~ casos4_,
                                (Casos >= casos2+1 & Casos <= casos3) ~ casos3_,
                                (Casos >= casos1+1 & Casos <= casos2) ~ casos2_,
                                (Casos >= 1 & Casos <= casos1) ~ casos1_,
                                (is.na(Casos) | Casos == 0) ~casos0_)) %>%
  arrange(desc(`Casos`)) %>%
  mutate(Categoria = factor(Categoria, levels =niveles)) %>%
  rename(`Pais ingles` = Pais_ing)


paste("Se acumulan un total de:",sum(Datos_Viruela$Casos, na.rm = T), "casos")


# -----------------------------------------------------------------------------#
#           4. DATOS DE VIRUELA DE OMS POR REGIÓN ####
#------------------------------------------------------------------------------#

Regiones_ <- Datos_Viruela %>%
  filter(Region_OMS != "Otros") %>%
  group_by(Region_OMS) %>%
  summarise(Casos_region = sum(Casos, na.rm = T)) %>%
  arrange(desc(Casos_region)) %>%
  mutate(Categoria_region = paste0(Region_OMS, ": ", Casos_region))

Regiones <- Regiones_ %>%
  mutate(Categoria_ = case_when( Casos_region >= casos5 ~ casos5_,
                                 (Casos_region >= casos3+1 & Casos_region <= casos4) ~ casos4_,
                                 (Casos_region >= casos2+1 & Casos_region <= casos3) ~ casos3_,
                                 (Casos_region >= casos1+1 & Casos_region <= casos2) ~ casos2_,
                                 (Casos_region >= 1 & Casos_region <= casos1) ~ casos1_,
                                 (is.na(Casos_region) | Casos_region == 0) ~casos0_)) %>%

  mutate(Categoria_region = factor(Categoria_region, levels = Regiones_$Categoria_region)) %>%
  mutate(Categoria_ = factor(Categoria_, levels = niveles)) %>%

  mutate(Color_region = case_when(Categoria_ == casos5_ ~ cat_1,
                                  Categoria_ == casos4_ ~ cat_2,
                                  Categoria_ == casos3_ ~ cat_3,
                                  Categoria_ == casos2_ ~ cat_4,
                                  Categoria_ == casos1_ ~ cat_5,
                                  Categoria_ == casos0_ ~ cat_6 ))

#Establecemos al vector de colores el nombre de la región a a que pertenece Esto
#debido a que necesitamos un vector cuya característica principal sea el color,
#pero su nombre de referencia y posición como vector se refiere a un valor
#categórico
names(Regiones$Color_region) <- Regiones$Categoria_region
colores_categorias_regiones <- Regiones$Color_region



# -----------------------------------------------------------------------------#
#           5. MAPA DE VIRUELA ####
#------------------------------------------------------------------------------#

#Unimos el dataframe de los países y la función correct_CountryNames()
Datos_Viruela <- correct_CountryNames(Datos_Viruela)

world <- map("world", fill=TRUE, plot=FALSE)
world_map <- map2SpatialPolygons(world, sub(":.*$", "", world$names))
world_map <- SpatialPolygonsDataFrame(world_map,
                                      data.frame(country=names(world_map),
                                                 stringsAsFactors=FALSE), FALSE)
#Hacemos ultimas correcciones al data frame del mapa
world_map@data$country[which(world_map@data$country %in% c("Turkey"))] <- "Türkiye"

# Hacemos el merge de Datos_Viruela con los rasters de los mapas
world_map@data<-left_join(world_map@data ,Datos_Viruela, by = c("country" = "Pais ingles") )

# Realizamos un filtro para seleccionar aquellos países que no se pintarán en el mapa
#world_map@data$country[!Datos_Viruela$`Pais ingles` %in%  world_map@data$country]


world_map@data <- world_map@data %>%
  mutate(Casos = ifelse(is.na(Casos), 0, Casos))

colores_categorias1 <- colorFactor(colores_categorias, Datos_Viruela$Categoria, ordered = T)
colores_categorias2 <- colorFactor(colores_categorias_regiones, Regiones$Categoria_region, ordered = T)



Mapa_casos_activos <-
  leaflet(options = leafletOptions(
    attributionControl=FALSE)) %>%
  setView(lng = 10, lat = 40, zoom = 2.4) %>%
  addProviderTiles(provider = "CartoDB.PositronNoLabels") %>%
  addPolygons(
    data = world_map,
    fillColor = colorFactor(colores_categorias, domain = world_map$Categoria)(world_map$Categoria),
    weight = 1,
    opacity = 1,
    color = "black",
    fillOpacity = 0.8,
    smoothFactor = 1,
    highlight = highlightOptions(
      weight = 1,
      color = "white",
      fillOpacity = .9,
      bringToFront = TRUE),
    label = ~paste(as.character(world_map@data$`Pais`),
                   "Casos Nuevos:",
                   as.character(prettyNum(`Casos`,
                                          big.mark=",",scientific=FALSE)))) %>%
  addLegendFactor(pal = colores_categorias1,
                values = Datos_Viruela$Categoria,
                labelStyle = htmltools::tags$div('Casos Nuevos', style = 'font-size: 12px; align="center"; color: Black; font-family: Montserrat'),
                position = "bottomright",
                title = htmltools::tags$div('Casos Nuevos', style = 'font-size: 12px; color: Black; font-family: Montserrat'),
                opacity = 1,
                orientation = c( "horizontal"),
                layerId = 'horizontal',
                width = 30,
                height = 30 )

Mapa_casos_activos

# -----------------------------------------------------------------------------#
#           5. GRÁFICOS DE VIRUELA ####
#------------------------------------------------------------------------------#

df<-data.frame(Color = rep("",6) , Categoría = niveles )

colores_tabla <- flextable::flextable(df) %>%
  align(align = "center", part = "all") %>%
  bg(i = 1, bg = "#2B614D", part = "header") %>%
  color(i = 1, color = "white", part = "header") %>%

  fontsize(., size = 11, part = "header") %>%
  bg(., bg = "#9D2348", part = "header") %>%
  bg(i = 1, j = 1, bg = cat_1) %>%
  bg(i = 2, j = 1, bg = cat_2) %>%
  bg(i = 3, j = 1, bg = cat_3) %>%
  bg(i = 4, j = 1, bg = cat_4) %>%
  bg(i = 5, j = 1, bg = cat_5) %>%
  bg(i = 6, j = 1, bg = cat_6) %>%

  border(part = "all", border = fp_border(color = "black", style = "solid", width = 2.5) ) %>%
  bold(part ="all") %>%
  font(fontname = "Montserrat", part = "all")


totales <- c("Total", sum(Datos_Viruela$Casos, na.rm = T))

df2 <- data.frame(
  "Región OMS" = (Regiones$Region_OMS) ,
  "Casos confirmados" = (Regiones$Casos_region),
  check.names = F) %>%
  janitor::adorn_totals(name = "Total")


regiones_tabla<-flextable::flextable(df2) %>%
  align(align = "center", part = "all") %>%
  bg(i = 1, bg = "#2B614D", part = "header") %>%
  color(i = 1, color = "white", part = "header") %>%
  fontsize(., size = 11, part = "header") %>%
  bg(., bg = "#9D2348", part = "header") %>%
  border(part = "all", border = fp_border(color = "black", style = "solid", width = 2.5) ) %>%
  bold(part ="all") %>%
  font(fontname = "Montserrat", part = "all")


prettyNum(Datos_Viruela$Casos %>% sum(., na.rm = T), big.mark = ",")
prettyNum(as.numeric(Datos_Viruela$Defunciones) %>% sum(., na.rm = T), big.mark = ",")

length(unique(Regiones$Region_OMS))

num_paises<-length(load_data_OMS()[2] %>%
                     as.data.frame() %>%
                     .$Pais_ing )

legend_factor <- as.character(Regiones$Region_OMS)

grafico_regiones <-
  Regiones %>%
  ggplot()+
  geom_bar(aes(y=reorder(Region_OMS, Casos_region), x= Casos_region,
               fill = factor(Region_OMS, levels = legend_factor) ), stat = "identity")+
  scale_fill_manual(values = colores_fact)+
  labs(title = "",
       fill = "Regiones OMS",
       y = "Regiones de la OMS",
       x = "Casos acumulados")+
  #labs(title = expression(paste(bold("Gráfico 1.")," Distribución de casos confirmados a Viruela Símica por regiones de la OMS")),
  cowplot::theme_minimal_hgrid()+
  theme(text=element_text(size=14,
                          family="Montserrat", ),
        axis.text.x = element_text(size = 18),
        axis.text.y = element_text(size = 21),
        axis.title.x = element_text(size = 19, family = "Montserrat", face = "bold", vjust = -1),
        axis.title.y = element_text(size = 19, family = "Montserrat", face = "bold", vjust = 3),
        legend.position = "none", legend.justification = "center")+
  scale_x_continuous(labels = scales::comma, limits = c(0,max(Regiones$Casos_region)+(max(Regiones$Casos_region)*.05)))+
  geom_text(
    aes(y=Region_OMS,
        x = Casos_region, label=prettyNum(Casos_region,big.mark = ",")),
    vjust=0,hjust=-.3, size=6, family="Montserrat", fontface='bold')


# -----------------------------------------------------------------------------#
#           6. GUARDANDO IMÁGENES ####
#------------------------------------------------------------------------------#
# Mapa Viruela
saveWidget(Mapa_casos_activos, "productos/Mapa_Viruela.html")
webshot2::webshot("productos/Mapa_Viruela.html", file="productos/Mapa_Viruela.png",
                  cliprect="viewport", vwidth = 1050, vheight = 680)

#Desarmamos la carpeta
unlink(paste0(path_Productos, "Mapa_Viruela_files"), recursive = TRUE)

#Grafico barras
ggsave(paste0(path_Productos, "grafico_regiones.png"),
       grafico_regiones,
       width=17, height=10,
       units = "cm", dpi=300, scale = 2.5)

#Imágenes de las tablas
save_as_image(width(regiones_tabla, width = 1.7),
              path = paste0(path_Productos,"tabla_regiones_viruela.png"), webshot = "webshot2")

save_as_image(width(colores_tabla, width = 1),
              path = paste0(path_Productos,"tabla_colores_viruela.png"), webshot = "webshot2")

